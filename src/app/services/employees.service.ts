import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../interfaces/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private http: HttpClient) { }

  obtenerEmpleados(): Observable<Array<Employee>>{
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
      })
    };

    return this.http.get<Array<Employee>>('http://localhost:8081/api/employees', httpOptions);
  }

  actualizarEmpleado(employee:Employee){
    console.log(employee)
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'Content-Type': 'application/json'
      })
    };

    return this.http.put(`http://localhost:8081/api/employee/${employee.id}`, employee, httpOptions).toPromise();
  }

  crearEmpleado(employee:Employee){
    console.log(employee)

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`http://localhost:8081/api/employee/create`, employee, httpOptions).toPromise();
  }

  deleteEmpleado(employee:Employee){
    console.log(employee)

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'Content-Type': 'application/json'
      })
    };

    return this.http.delete(`http://localhost:8081/api/employee/${employee.id}/delete`, httpOptions).toPromise();
  }

}
