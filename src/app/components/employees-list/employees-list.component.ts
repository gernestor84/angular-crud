import { Component, OnInit } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { EmployeesService } from 'src/app/services/employees.service';
import { Employee } from 'src/app/interfaces/employee';
import { EmployeeModalComponent } from '../employee-modal/employee-modal.component';


@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {
  
  employees:Array<Employee>;
  
  constructor(private _employeeService:EmployeesService, public dialog: MatDialog) { }

  ngOnInit() {
    this.employees_list();
  }

  employees_list(){
    this._employeeService.obtenerEmpleados().subscribe(result=>{
      this.employees = result
    });
  }

  createEmployee(){
    const dialogRef = this.dialog.open(EmployeeModalComponent, {
      width: '600px',
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.employees_list();
    });
  }

  editDialog(employee:Employee){
    const dialogRef = this.dialog.open(EmployeeModalComponent, {
      width: '600px',
      data: employee
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.employees_list();
    });
  }
  
  delete(employee:Employee){
    this._employeeService.deleteEmpleado(employee).then(()=>{
      console.log('Usuario borrado')
      this.employees_list();
    });
  }

}
