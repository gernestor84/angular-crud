import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Employee } from 'src/app/interfaces/employee';
import { EmployeesService } from 'src/app/services/employees.service';
@Component({
  selector: 'app-employee-modal',
  templateUrl: './employee-modal.component.html',
  styleUrls: ['./employee-modal.component.css']
})
export class EmployeeModalComponent implements OnInit{

  employee:Employee;

  constructor(
                public dialogRef: MatDialogRef<EmployeeModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: Employee, private _employeeService:EmployeesService
              ) {
                console.log(data)
                if(data != null){
                  this.employee = data
                }else{
                  this.employee = {
                    id:0,
                    name:'',
                    last_name:'',
                    email:''
                  }
                }
                  
                 // console.log(this.employee.email)
    }

    saveEmployee(){
      console.log('saveEmployee')
      if(this.employee.id == 0){
        this._employeeService.crearEmpleado(this.employee).then(()=>{
          console.log("Creado")
        });
      }else{
        this._employeeService.actualizarEmpleado(this.employee).then(()=>{
          console.log("Actualizado")
        });
      }
    }

    ngOnInit():void {
      
    }

}
